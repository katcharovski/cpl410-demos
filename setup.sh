#!/bin/bash

#
# Installer Script for CPL410-Demos
#

##### Constants

USER="$( who | awk '{print $1}')"
ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
LOGDIR=$ROOTDIR/.ansible/logs
DATE=`date "+%Y%m%d%H%M%S"`
LOGFILE=$LOGDIR/install-$DATE.log
IPADDRESS=$(ip -4 addr show enp1s0| grep -oP '(?<=inet\s)\d+(\.\d+){3}')

##### Functions

installation_failed()
{
cat << "FAILED"
   ____  _            ____  _     
  / __ \| |          / __ \| |    
 | |  | | |__ ______| |  | | |__  
 | |  | | '_ \______| |  | | '_ \ 
 | |__| | | | |     | |__| | | | |
  \____/|_| |_|      \____/|_| |_|
FAILED
echo It appears as though something has gone wrong. The logfile located at
echo $LOGFILE may give you a hint as to what went wrong.  You can also try 
echo contacting the owners of the source repository for guidance or try the 
echo installation again. Please be sure to have your logfile handy if you are
echo contacting the repository owners as they will need the information contained
echo within.
echo 
echo Sorry
}

installation_successful()
{
cat << "SUCCESS"
  ___         _        _ _      _   _             ___                _     _       
 |_ _|_ _  __| |_ __ _| | |__ _| |_(_)___ _ _    / __|___ _ __  _ __| |___| |_ ___ 
  | || ' \(_-<  _/ _` | | / _` |  _| / _ \ ' \  | (__/ _ \ '  \| '_ \ / -_)  _/ -_)
 |___|_||_/__/\__\__,_|_|_\__,_|\__|_\___/_||_|  \___\___/_|_|_| .__/_\___|\__\___|
                                                               |_|                 

SUCCESS
echo You can now point your web browser at: http://$IPADDRESS to start using your newly
echo provisioned system.  Issues can be logged on the bitbucket site and there is also
echo additional documentation on the Wiki.  We hope you like the demos and are always
echo interested in feedback.
}

splash_screen()
{
cat << "BEGIN"
  _____ _____  _   _  _  __  ___         _____                           
 / ____|  __ \| | | || |/_ |/ _ \       |  __ \                          
| |    | |__) | | | || |_| | | | |______| |  | | ___ _ __ ___   ___  ___ 
| |    |  ___/| | |__   _| | | | |______| |  | |/ _ \ '_ ` _ \ / _ \/ __|
| |____| |    | |____| | | | |_| |      | |__| |  __/ | | | | | (_) \__ \
 \_____|_|    |______|_| |_|\___/       |_____/ \___|_| |_| |_|\___/|___/

Brought to you by:

  ___                          _       _                  _   _          
 / __|___ ___ __ __ _ _ _     /_\ _  _| |_ ___ _ __  __ _| |_(_)___ _ _  
| (_ / -_|_-</ _/ _` | ' \   / _ \ || |  _/ _ \ '  \/ _` |  _| / _ \ ' \ 
 \___\___/__/\__\__,_|_||_| /_/ \_\_,_|\__\___/_|_|_\__,_|\__|_\___/_||_|
                                          _ 
                             __ _ _ _  __| |
                            / _` | ' \/ _` |
                            \__,_|_||_\__,_|
  ___ _      _              _       _                  _   _          
 / __(_)_ __| |_ ___ __    /_\ _  _| |_ ___ _ __  __ _| |_(_)___ _ _  
| (__| | '  \  _/ -_) _|  / _ \ || |  _/ _ \ '  \/ _` |  _| / _ \ ' \ 
 \___|_|_|_|_\__\___\__| /_/ \_\_,_|\__\___/_|_|_\__,_|\__|_\___/_||_|

https://bitbucket.org/gescancimtec/cpl410-demos/src

Preparing Installation ...
BEGIN
} # end of splash_screen

usage()
{
    echo "usage: sudo ./setup.sh -vh"
    echo 
    echo "Did you forget to sudo?"
    echo
    echo "Short options:		GNU long options: (extensions)"
    echo "	-v			--verbose"
    echo "	-h			--help"
}

##### Start Script

while [ "$1" != "" ]; do
    case $1 in
        -v | --verbose )        VERBOSE=1
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

clear
if [ "$(whoami)" != "root" ]; then
usage
exit 126
fi
splash_screen
sleep 5
mkdir -p $LOGDIR
touch $LOGFILE
cd $ROOTDIR
echo IP Address: $IPADDRESS >> $LOGFILE
echo Updating apt ... | tee -a $LOGFILE
apt-get update >> $LOGFILE 2>&1
echo Upgrading system software ... | tee -a $LOGFILE
apt-get -y upgrade >> $LOGFILE 2>&1
echo Upgrading pip to latest ... | tee -a $LOGFILE
pip install --upgrade pip >> $LOGFILE 2>&1 &
wait %1
echo Adding installer to the system ... | tee -a $LOGFILE
pip install ansible jmespath >> $LOGFILE 2>&1
echo Setting system timezone ... | tee -a $LOGFILE
dpkg-reconfigure tzdata 2>> $LOGFILE
echo Running installer ... | tee -a $LOGFILE
ansible-playbook .ansible/install.yml -i .ansible/hosts --extra-vars "user=$USER" | tee -a $LOGFILE
clear
FAILURES=$(cat $LOGFILE | grep failed= | awk {'print $6'} | cut -d '=' -f2)
if [ $FAILURES -eq 0 ]
then
installation_successful
else
installation_failed
fi
