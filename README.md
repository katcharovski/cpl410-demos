# CPL410

This repository contains sample code and instllation scripts to get a GE CPL410 up and running quickly.  Very little linux knowledge/experience is required in order to use these demo scripts.

## Repository Contents

This repository contains the following folders:

### Grafana

This folder contains all of the grafana dashboards for the sample systems.

### Machine Edition

This folder contains the Proficy Machine Edition project file required for the demo's to properly function.  It was created in ME V9.5 SIM8

### Python

This folder contains several sample scripts written using the Python language.  For demonstative purposes we have used a reusable common set of functions.  Scripts include:

- #### Analytics
A small sample analytics engine using pandas. (Incomplete)
- #### Calculations
Application that shows a constant loop in python doing a simple read from the controller, run the value through a calculation, and write back to the controller.  (Incomplete)
- #### CHC
Controller health check application.  Reads values from controller and writes them to influxDB.  Works with Grafana dashboard for presentation.
- #### Price of Power
Scrapes 5 minute price of power, and 5 minute demand from the [IESO Website](http://www.ieso.ca "Link to Website") and logs it to influxDB.
- #### Weather
Uses yahoo API to read weather data and logs it to influxDB as well as passes data to controller UDT.

## Installation

Installation of the demo system is simple.  First ensure your CPL410 has been factory reset.  Consult the GE documentation for how to factory reset your device.  Once the CPL410 has been factory reset, log into your device using the default username (ge) with the default password (ge).  It is reccomended you immediately change your password by typing `passwd` from the command prompt.  You will be prompted to put in your current password, and then asked to type in a new password, and repeat the password.

In order for the next series of steps to function properly your CPL410 must be connected to the internet.

Once you have changed your password and connected the CPL410 to the internet you can install the demos with the following code:

```Shell
cd ~
git clone https://bitbucket.org/gescancimtec/cpl410-demos.git
cd cpl410-demos/
sudo ./setup.sh
```

Thats it!  Wait patiently for the installation to finish and then point your web browser at the IP address of your linux partition.