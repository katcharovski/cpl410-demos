#!/usr/bin/python

import argparse
import signal
import json
import time
import logging
import paho.mqtt.client as mqtt
from opcua import Client
from common.cplcommon import getlogin

class AlarmServer:
    def __init__(self, config):
        user, password = getlogin('mosquitto')
        self.client = self.setupClient()
        self.tags = self.loadConfig(config)
        self.mqttClient = self.setupMqtt(user, password)
        self.interrupted = False
        signal.signal(signal.SIGINT, self.signalHandler)

    def compareValues(self, compare, val1, val2):
        return {
            'boolean': 1 if val1 == val2 else 0,
            'gt': 1 if val1 > val2 else 0,
            'gte': 1 if val1 >= val2 else 0,
            'lt': 1 if val1 < val2 else 0,
            'lte': 1 if val1 <= val2 else 0,
            'eq': 1 if val1 == val2 else 0,
            'ne': 1 if val1 != val2 else 0
        }[compare]

    def loadConfig(self, config):
        with open(config) as f:
            data = json.load(f)

        for index, item in enumerate(data["tags"]):
            item[u"ack"] = 0
            item[u"alm"] = 0
            item[u"cv"] = 0

        return data

    def mqttConnect(self, client, userdata, flags, rc):
        client.subscribe("alarms/ack")
        client.subscribe("alarms/newclient")

    def mqttMessage(self, client, userdata, msg):
        if msg.topic == 'alarms/ack':
            self.tags['tags'][int(msg.payload)][u"ack"] = 1
        elif msg.topic == "alarms/newclient":
            client.publish("alarms/sync", json.dumps(self.tags))

    def run(self):
        try:
            self.client.connect()
            self.mqttClient.connect("localhost")
            self.mqttClient.loop_start()

            while True:
                for index, item in enumerate(self.tags['tags']):
                    item[u"cv"] = self.client.get_node(str(item[u"address"])).get_value()
                    for alarmIndex, alarmItem in enumerate(item[u"alarms"]):
                        almState = self.compareValues(alarmItem[u"type"], item[u"cv"], alarmItem[u"value"])
                        if almState and item[u"alm"] == 0:
                            item[u"alm"] = 1
                            self.mqttClient.publish("alarms/alm", index)
                            break
                        if not almState and item[u"alm"] == 1:
                            item[u"alm"] = 0
                            self.mqttClient.publish("alarms/rtn", index)

                time.sleep(1)

                if self.interrupted:
                    break
        finally:
            self.mqttClient.loop_stop(force=False)
            self.mqttClient.disconnect()
            self.client.disconnect()


    def setupClient(self):
        return Client("opc.tcp://192.168.180.2:4840")

    def setupMqtt(self, user, password):
        mqttClient = mqtt.Client()
        mqttClient.username_pw_set(user, password)
        mqttClient.on_connect = self.mqttConnect
        mqttClient.on_message = self.mqttMessage
        return  mqttClient

    def signalHandler(self, signal, frame):
        self.interrupted = True

def parse_args():
    parser = argparse.ArgumentParser(
        description='Example Alarm Anunciation Backend Script')
    parser.add_argument('--config', type=str, required=False,
                        default='./alarmconfig.json',
                        help='path to the configuration file')
    return parser.parse_args()

if __name__ == '__main__':
    args = parse_args()
    server = AlarmServer(args.config)
    server.run()
