#!/usr/bin/python

# import libraries
import argparse
import urllib2, urllib, json
import datetime
from dateutil.parser import parse
from influxdb import InfluxDBClient
from opcua import Client
from opcua import ua
from common.cplcommon import getlogin, getWoeId

def main(host='localhost', port=8086):
    """Instantiate a connection to the InfluxDB."""
    weatherData = getWeatherData()
    user, password = getlogin('influxdb')
    dbname = 'weather'
    json_body = [
        {
            "measurement": "weather",
            "time": parse(weatherData['query']['created']).isoformat(),
            "fields": {
                "location": weatherData['query']['results']['channel']['location']['city'] + "," + weatherData['query']['results']['channel']['location']['region'],
                "temperature_units": weatherData['query']['results']['channel']['units']['temperature'],
                "wind_units": weatherData['query']['results']['channel']['units']['speed'],
                "temperature": int(weatherData['query']['results']['channel']['item']['condition']['temp']),
                "humidity": int(weatherData['query']['results']['channel']['atmosphere']['humidity']),
                "atmosphere_pressure": float(weatherData['query']['results']['channel']['atmosphere']['pressure']),
                "wind_speed": float(weatherData['query']['results']['channel']['wind']['speed']),
                "wind_direction": int(weatherData['query']['results']['channel']['wind']['direction']),
                "wind_chill": int(weatherData['query']['results']['channel']['wind']['chill']),
                "current_conditions": int(weatherData['query']['results']['channel']['item']['condition']['code']),
            }
        }
    ]

    client = InfluxDBClient(host, port, user, password, dbname)
    client.write_points(json_body)
    updateController(weatherData)

def getWeatherData():
    baseurl = "https://query.yahooapis.com/v1/public/yql?"
    woeid = getWoeId().split(',')
    yql_query = "select * from weather.forecast where woeid="+woeid[0]+" and u='"+woeid[1]+"'"
    yql_url = baseurl + urllib.urlencode({'q':yql_query}) + "&format=json"
    result = urllib2.urlopen(yql_url).read()
    data = json.loads(result)
    return data

def updateController(weatherData):
    myClient = setupClient()
    try:
        myClient.connect()
        locationTag = myClient.get_node("ns=2;s=WeatherData.location")
        temperatureUnitsTag = myClient.get_node("ns=2;s=WeatherData.units")
        windUnitsTag = myClient.get_node("ns=2;s=WeatherData.wind.units")
        temperatureTag = myClient.get_node("ns=2;s=WeatherData.temperature")
        humidityTag = myClient.get_node("ns=2;s=WeatherData.humidity")
        apTag = myClient.get_node("ns=2;s=WeatherData.atmosphericPressure")
        wsTag = myClient.get_node("ns=2;s=WeatherData.wind.speed")
        wdTag = myClient.get_node("ns=2;s=WeatherData.wind.direction")
        wcTag = myClient.get_node("ns=2;s=WeatherData.wind.chill")
        ccTag = myClient.get_node("ns=2;s=WeatherData.currentConditions")

        locationTag.set_value(weatherData['query']['results']['channel']['location']['city'] + "," + weatherData['query']['results']['channel']['location']['region'],ua.VariantType.String)
        temperatureUnitsTag.set_value(weatherData['query']['results']['channel']['units']['temperature'],ua.VariantType.String)
        windUnitsTag.set_value(weatherData['query']['results']['channel']['units']['speed'],ua.VariantType.String)
        temperatureTag.set_value(int(weatherData['query']['results']['channel']['item']['condition']['temp']),ua.VariantType.Int16)
        humidityTag.set_value(int(weatherData['query']['results']['channel']['atmosphere']['humidity']),ua.VariantType.Int16)
        apTag.set_value(float(weatherData['query']['results']['channel']['atmosphere']['pressure']),ua.VariantType.Float)
        wsTag.set_value(float(weatherData['query']['results']['channel']['wind']['speed']),ua.VariantType.Float)
        wdTag.set_value(int(weatherData['query']['results']['channel']['wind']['direction']),ua.VariantType.Int16)
        wcTag.set_value(int(weatherData['query']['results']['channel']['wind']['chill']),ua.VariantType.Int16)
        ccTag.set_value(int(weatherData['query']['results']['channel']['item']['condition']['code']),ua.VariantType.Int16)

    finally:
        myClient.disconnect()
        return 1

def setupClient():
    return Client("opc.tcp://192.168.180.2:4840")

def parse_args():
    """Parse the args."""
    parser = argparse.ArgumentParser(
        description='example code to play with InfluxDB')
    parser.add_argument('--host', type=str, required=False,
                        default='localhost',
                        help='hostname of InfluxDB http API')
    parser.add_argument('--port', type=int, required=False, default=8086,
                        help='port of InfluxDB http API')
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    main(host=args.host, port=args.port)


