#!/usr/bin/python

import argparse
from opcua import Client

def main(item='CatalogNumber'):
    client = setupClient()
    try:
        client.connect()

        # Only Grab the varible we are interested in
        if item == 'CatalogNumber':
            itemAddress = "ns=2;i=20102"
        elif item == 'CpuStatus':
            itemAddress = "ns=2;s=MFA_DATA.CPU_STS"
        elif item == 'FaultString':
            itemAddress = "ns=2;s=MFA_DATA.FLT_STR"
        elif item == 'Scan10mAverage':
      	    itemAddress = "ns=2;s=MFA_DATA.SCN_10m_AVG"
        elif item == 'Scan10mMax':
           itemAddress = "ns=2;s=MFA_DATA.SCN_10m_MAX"
        elif item == 'Scan60mAverage':
           itemAddress = "ns=2;s=MFA_DATA.SCN_60m_AVG"
        elif item == 'Scan60mMax':
           itemAddress = "ns=2;s=MFA_DATA.SCN_60m_MAX"
        elif item == 'Scan60sAverage':
           itemAddress = "ns=2;s=MFA_DATA.SCN_60s_AVG"
        elif item == 'Scan60sMax':
           itemAddress = "ns=2;s=MFA_DATA.SCN_60s_MAX"
        elif item == 'FirmwareVersion':
           itemAddress = "ns=2;i=20106"     
        elif item == 'DateCode':
           itemAddress = "ns=2;i=20104"
        elif item == 'HardwareVersion':
       	    itemAddress = "ns=2;i=20105"
        elif item == 'SerialNumber':
           itemAddress = "ns=2;i=20103"
        elif item == 'SweepMode':
           itemAddress = "ns=2;i=20107"  
        elif item == 'SweepTime':
           itemAddress = "ns=2;i=20108"
        else:
            itemAddress = "ns=2;i=20102"

	itemValue = client.get_node(itemAddress)
    	print(itemValue.get_value())

    finally:
	client.disconnect()

def setupClient():
    return Client("opc.tcp://192.168.180.2:4840")

def parse_args():
    """Parse the args."""
    parser = argparse.ArgumentParser(
        description='Python CHC script to get SNMP Variables')
    parser.add_argument('--item', type=str, required=True,
                        default='CatalogNumber',
                        help='What CHC variable do you want?')
    return parser.parse_args()

if __name__ == '__main__':
    args = parse_args()
    main(item=args.item)
