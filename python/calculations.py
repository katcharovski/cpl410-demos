#!/usr/bin/python

# import libraries
import argparse
import signal
import time
from opcua import Client

def main():
    signal.signal(signal.SIGINT, signalHandler)
    client = setupClient()
    try:
        client.connect()
        while True:
            # get a specific node knowing its node id
            catalog = client.get_node("ns=2;i=20102")
            print(catalog.get_value()) # get value of node as a python builtin

            serial = client.get_node("ns=2;i=20103")
            print(serial.get_value()) # get value of node as a python builtin

            fw = client.get_node("ns=2;i=20106")
            print(fw.get_value()) # get value of node as a python builtin

            sweep = client.get_node("ns=2;i=20108")
            print(sweep.get_value()) # get value of node as a python builtin


            # NOTE BELOW FOR SETTING VALUES, NEED TO SEND EXPLICIT TYPE
            #var.set_value(ua.Variant([23], ua.VariantType.Int64)) #set node value using explicit data type
            #var.set_value(3.9) # set node value using implicit data type

            time.sleep(3)

            if interrupted:
                print("Gotta Go")
                break
    finally:
        client.disconnect()

def setupClient():
    return Client("opc.tcp://192.168.180.2:4840")

def signalHandler(signal, frame):
    global interrupted
    interrupted = True

def parse_args():
    """Parse the args."""
    parser = argparse.ArgumentParser(
        description='example code to play with InfluxDB')
    return parser.parse_args()

if __name__ == '__main__':
    args = parse_args()
    interrupted = False
    main()


