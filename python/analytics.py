#!/usr/bin/python

# import libraries
import argparse
import pandas as pd
#import matplotlib.pylab as plt
#%matplotlib inline
#from matplotlib.pylab import rcParams
#rcParams['figure.figsize'] = 15, 6
from influxdb import DataFrameClient
from common.cplcommon import getlogin

def main(host='localhost', port=8086):
    """Instantiate a connection to the InfluxDB."""
    user, password = getlogin('influxdb')
    dbname = 'chc'
    protocol = 'json'
    client = DataFrameClient(host, port, user, password, dbname)

    print("Read DataFrame")
    datasets = client.query("select * from Sweep_Time LIMIT 1000")
    dataset = datasets['Sweep_Time']
    print("\nCount")
    print(dataset.count())
    print("\nSum")
    print(dataset.sum())
    print("\nMean")
    print(dataset.mean())
    print("\nMean Absolute Deviation")
    print(dataset.mad())
    print("\nMedian")
    print(dataset.median())
    print("\nMin")
    print(dataset.min())
    print("\nMax")
    print(dataset.max())
    print("\nMode")
    print(dataset.mode())
    #print("\nABS")
    #print(dataset.abs())
    print("\nProduct of Values")
    print(dataset.prod())
    print("\nStandard Deviation")
    print(dataset.std())
    print("\nUnbiased Variance")
    print(dataset.var())
    print("\nStandard Error of Mean")
    print(dataset.sem())
    print("\nSample Skewness")
    print(dataset.skew())
    print("\nSample Kurtosis")
    print(dataset.kurt())
    print("\nSample Quantile")
    print(dataset.quantile())
    #print("Index")
    #print dataset.index
    #print("Head")
    #print(dataset.head(10))
    #print("Mean")
    #print(dataset.mean())
    #print("Missing Samples")
    #print(dataset.isnull().sum())
    #ts = dataset['value'] 
    #ts.head()
    #print("Sample at 2018-03-08 12:42:16+00:00")
    #print(ts['2018-03-08 12:42:16+00:00'])
    #print("All Values at 13:00")
    #print(ts['2018-03-08 13'])

def parse_args():
    """Parse the args."""
    parser = argparse.ArgumentParser(
        description='example code to play with InfluxDB')
    parser.add_argument('--host', type=str, required=False,
                        default='localhost',
                        help='hostname of InfluxDB http API')
    parser.add_argument('--port', type=int, required=False, default=8086,
                        help='port of InfluxDB http API')
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    main(host=args.host, port=args.port)


