#!/usr/bin/python

# import libraries
import argparse
import datetime
import dateutil
import urllib2
import xml.etree.ElementTree as ET
from opcua import Client
from opcua import ua
from influxdb import InfluxDBClient
from common.cplcommon import getlogin

def main(host='localhost', port=8086):
    """Instantiate a connection to the InfluxDB."""
    user, password = getlogin('influxdb')
    dbname = 'power'
    client = InfluxDBClient(host, port, user, password, dbname)
    processPrice(client)
    processDemand(client)
    updateController(client)

def processPrice(client):
    # http://reports.ieso.ca/public/RealtimeMktPrice/PUB_RealtimeMktPrice.xml
    # this is a 5 minute report for energy price

    url = 'http://reports.ieso.ca/public/RealtimeMktPrice/PUB_RealtimeMktPrice.xml'
    page = urllib2.urlopen(url)
    tree = ET.parse(page)
    root = tree.getroot()
    docBody = root.find('{http://www.ieso.ca/schema}DocBody')
    deliveryDate = docBody.find('{http://www.ieso.ca/schema}DeliveryDate')
    dateObject = dateutil.parser.parse(deliveryDate.text).date()
    deliveryHour = docBody.find('{http://www.ieso.ca/schema}DeliveryHour')
    timeObject = datetime.time(int(deliveryHour.text)+4, 0, 0, 0) # +4 to offset UTC
    for zone in docBody.findall('{http://www.ieso.ca/schema}IntertieZonalPrices'):
      zoneName = zone.find('{http://www.ieso.ca/schema}IntertieZoneName')
      if zoneName.text == 'Ontario':
        for prices in zone.findall('{http://www.ieso.ca/schema}Prices'):
          priceType = prices.find('{http://www.ieso.ca/schema}PriceType')
          if priceType.text == 'Energy':
            for intervalPrice in prices.findall('{http://www.ieso.ca/schema}IntervalPrice'):
              interval = intervalPrice.find('{http://www.ieso.ca/schema}Interval')
              mcp = intervalPrice.find('{http://www.ieso.ca/schema}MCP')
              timeObject = datetime.time(int(deliveryHour.text)+4, (int(interval.text)-1)*5, 0, 0) # +4 to offset DST
              dbDate = datetime.datetime.combine(dateObject, timeObject)
              json_body = [
                  {
                      "measurement": "price",
                      "time": dbDate.isoformat(),
                      "fields": {
                          "value": float(mcp.text),
                      }
                  }
              ]
              client.write_points(json_body)

def processDemand(client):
    # http://reports.ieso.ca/public/RealtimeConstTotals/PUB_RealtimeConstTotals.xml
    # this is a  5 minute report for demand

    dateObject =  datetime.date.today()
    url = 'http://reports.ieso.ca/public/RealtimeConstTotals/PUB_RealtimeConstTotals.xml'
    page = urllib2.urlopen(url)
    tree = ET.parse(page)
    root = tree.getroot()
    docBody = root.find('{http://www.ieso.ca/schema}DocBody')
    deliveryDate = docBody.find('{http://www.ieso.ca/schema}DeliveryDate')
    dateObject = dateutil.parser.parse(deliveryDate.text).date()
    deliveryHour = docBody.find('{http://www.ieso.ca/schema}DeliveryHour')
    energies = docBody.find('{http://www.ieso.ca/schema}Energies')
    for intervalEnergy in energies.findall('{http://www.ieso.ca/schema}IntervalEnergy'):
      interval = intervalEnergy.find('{http://www.ieso.ca/schema}Interval')
      timeObject = datetime.time(int(deliveryHour.text)+4, (int(interval.text)-1)*5, 0, 0) # +4 to offset DST
      for mq in intervalEnergy.findall('{http://www.ieso.ca/schema}MQ'):
        marketquantity = mq.find('{http://www.ieso.ca/schema}MarketQuantity')
        energymw = mq.find('{http://www.ieso.ca/schema}EnergyMW')
        if marketquantity.text == 'ONTARIO DEMAND':
          dbDate = datetime.datetime.combine(dateObject, timeObject)
          json_body = [
              {
                  "measurement": "demand",
                  "time": dbDate.isoformat(),
                  "fields": {
                      "value": float(energymw.text),
                  }
              }
          ]
          client.write_points(json_body)

def updateController(client):
    myClient = setupClient()
    try:
        myClient.connect()
        priceTag = myClient.get_node("ns=2;s=PowerData.fiveMinutePrice")
        demandTag = myClient.get_node("ns=2;s=PowerData.fiveMinuteDemand")

        query = 'select last(value) from price;'
        result = client.query(query)
        item = result[('price', None)]
        for i in item:
            price = i['last']
        priceTag.set_value(float(price),ua.VariantType.Float)
        query = 'select last(value) from demand;'
        result = client.query(query)
        item = result[('demand', None)]
        for i in item:
            demand = i['last']
        demandTag.set_value(float(demand),ua.VariantType.Float)
    
    finally:
        myClient.disconnect()

def setupClient():
    return Client("opc.tcp://192.168.180.2:4840")

def parse_args():
    """Parse the args."""
    parser = argparse.ArgumentParser(
        description='example code to play with InfluxDB')
    parser.add_argument('--host', type=str, required=False,
                        default='localhost',
                        help='hostname of InfluxDB http API')
    parser.add_argument('--port', type=int, required=False, default=8086,
                        help='port of InfluxDB http API')
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    main(host=args.host, port=args.port)


